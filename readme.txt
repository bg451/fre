----- COMPILING -------
To compile the program, make sure you are on Mac OS x or linux. Please note
that this has been tested on OS X and Ubuntu 14.04 using g++.

- GNU g++ version 4.9 or higher is required.
  - IF YOU ARE USING UBUNTU: `apt-get install g++` latest version is 4.8.
    In order to install verion 4.9, check out this link.
    http://askubuntu.com/questions/428198/getting-installing-gcc-g-4-9-on-ubuntu
    After installing g++-4.9, you need to symlink it to g++ or run
    `make CC=g++-4.9` or whatever your g++-VERSION is.
- make is required as well.

To compile, run `make`. This will compile and generate a binary

----- RUNNING -------
To run the program, run `./scorer`. It will prompt you for a filename.
To quit the program, type "QUIT" or ctrl-c. If a valid file is provided, the
program will display the scores. Metrics will be written to output.txt
