#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <regex>
#include <string>
#include <vector>
#include <sys/stat.h>

#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;

// TextMetrics is a class that returns certain metrics for text
// such as the word count, sentence count, etc.
class TextMetrics {
  std::string text_;
  std::regex syl_re;

  public:
  // TextMetrics(string) creates a new object with your string as the text.
  TextMetrics(std::string text) {
    syl_re = std::regex("(?!e\\b)[YAEIOUyaeiou]+"); // these all qualify as syllables. Ignore any e's that end the sentence
    text_ = text;
  };

  int count_words() {
    int count = 1;
    int state = 0;
    std::string::iterator it;
    for (it = this->text_.begin();  it != text_.end(); ++it) {
      if (*it == ' ' || *it == '\n' || *it == '\r' || *it == '\t') {
        state = 1; // we are reading white space
      } else if (state == 1) { // if we're in whitespace mode but encountered a char, inc the count
        state = 0; // we are now reading actual characters
        count++;
      }
    }
    return count;
  };

  int count_sentences() {
    std::regex delimiters("[.?!:;]");
    return count_all_matches(text_, delimiters);
  };

  // If a word is like fly, we still count it as one syllable even though it
  // doesn't have any vowels.
  int count_syllables() {
    int count = 0; // count
    std::vector<std::string> words = split(text_, ' ');
    for (int i=0; i < words.size(); i++) {
      int syl_count = count_all_matches(words[i], syl_re);
      if (syl_count > 0) {
        count += syl_count;
      } else {
        count += 1;
      }
    }
    return count;
  };

  int count_poly_syllables() {
    std::vector<std::string> words = split(text_, ' ');
    int count = 0;
    for (int i = 0; i < words.size(); ++i) {
      if (count_all_matches(words[i], syl_re) > 2 ){
        ++count;
      }
    }
    return count;
  };

  private:
  // Given a regex, count_all_matches will count every single regex match
  int count_all_matches(std::string data, std::regex re) {
    std::regex_iterator<std::string::iterator> it (data.begin(),
        data.end(),
        re);
    std::regex_iterator<std::string::iterator> end;
    int c = 0;
    while (it != end) {
      ++c;
      ++it;
    }
    return c;
  };

  // http://stackoverflow.com/questions/236129/split-a-string-in-c
  std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
      elems.push_back(item);
    }
    return elems;
  };

  std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
  };
};

class Scorer {
  TextMetrics my_stats;

  public:
  Scorer(std::string text) : my_stats(text) {};

  // fre_score returns the FRE score and grade.
  // A pointer to an int must be passed in to store the grade.
  float fre_score(int* grade) {
    float score = 206.835 -
      1.015*my_stats.count_words()/my_stats.count_sentences() -
      84.6*my_stats.count_syllables()/my_stats.count_words();
    if (score > 100) {
      *grade = 14;
    } if (score <= 100 && score >= 91) {
      *grade = 5;
    } else if (score < 91 && score >= 81) {
      *grade = 6;
    } else if (score < 81 && score >= 71) {
      *grade = 7;
    } else if (score < 71 && score >= 66) {
      *grade = 8;
    } else if (score < 66 && score >= 61) {
      *grade = 9;
    } else if (score < 61 && score >= 51) {
      *grade = 10;
    } else if (score < 51 && score >= 31) {
      *grade = 11;
    } else if (score < 31 && score >= 0) {
      *grade = 12;
    } else if (score < 0) {
      *grade = 13;
    }
    return score;
  };

  // fkra_score() return the FKRA score of a given piece of text.
  float fkra_score() {
    return 11.8*my_stats.count_syllables()/my_stats.count_words() +
      0.39*my_stats.count_words()/my_stats.count_sentences() - 15.59;
  };

  // smog_score() returns the SMOG score of a given piece of text
  float smog_score(int* grade) {
    float indicator = (my_stats.count_poly_syllables() / static_cast<float>(my_stats.count_sentences())) * 30.0;
    float grade_level = std::round(std::sqrt(indicator)) + 3.0;
    if(grade_level > 13) {
      *grade = 13;
    } else {
      *grade = static_cast<int>(grade_level);
    }
    return indicator;
  };

  // Writes metrics to a file
  void write_output_file(std::string out_name, std::string in_name) {
    int grade;
    std::ofstream output_file;
    output_file.open(out_name, std::ofstream::out); // append
    output_file << in_name << "\t"
      << my_stats.count_sentences() << "\t"
      << my_stats.count_words() << "\t"
      << my_stats.count_syllables() << "\t"
      << my_stats.count_poly_syllables() << "\t"
      << fre_score(&grade) << "\t"
      << fkra_score() << "\t"
      << smog_score(&grade) << "\n";
  }
};

std::string read_file(std::string file_name) {
  std::ifstream input_file(file_name); // Open up our file
  input_file.seekg(0, input_file.end); // Set position to end of file
  int buffer_size = input_file.tellg(); // Get the end's position, so total file size
  char* buffer = new char[buffer_size]; // create a buffer
  input_file.seekg(0, input_file.beg); // Seek back to the beginning
  input_file.read(buffer, buffer_size); // Copy text into the buffer
  std::string text = std::string(buffer, buffer_size); // Convert the buffer to a string
  return text;
}

void print_files() {
  fs::path current_path(fs::initial_path<fs::path>());
  fs::directory_iterator end_iter;
  std::cout << "Current files in your directory" << std::endl;
  std::cout << "-------------------------------" << std::endl;
  for (fs::directory_iterator iter(current_path); iter != end_iter; ++iter){
    std::cout << "- " << iter->path().filename().c_str() << std::endl;
  };
  return;
}

int main() {
  std::string grades[] = {"a first grade", "a second grade", "a third grade",
    "a fourth grade", "a fifth grade", "a sixth grade", "a seventh grade",
    "an eigth grade", "a ninth grade", "a highschool level", "a college level",
    "a college graduate level", "a law school graduate level", "less than a fifth grade"};

  std::string smog_grades[] = {"a first grade", "a second grade", "a third grade",
    "a fourth grade", "a fifth grade", "a sixth grade", "a seventh grade",
    "an eigth grade", "a ninth grade", "a tenth grade", "an eleventh grade",
    "a twelfth grade", "a college" };


  while(1) {
    print_files();
    std::cout << "Please enter your filename or QUIT to exit: ";
    std::string file_name;
    std::cin >> file_name;
    int grade;
    struct stat buff;

    if (file_name == "QUIT") {
      return 0;
    } else if (file_name.substr(file_name.find_last_of(".") + 1) != "txt") {
      std::cout << file_name << " has an invalid extension. \".txt\" is required.\n";
      continue;
    } else if (stat(file_name.c_str(), &buff) != 0) {
      std::cout << "There has been an error opening " << file_name << "\n";
      continue;
    }

    Scorer s(read_file(file_name));

    float _fre_score = s.fre_score(&grade);
    std::cout << "The file \"" << file_name << "\" has a FRE Score of " << _fre_score <<
      ", indicating it requires " << grades[grade-1]<< " education to read.\n";

    std::cout << "Its FKRA Score is " << s.fkra_score() << "\n";

    float _smog_score_indicator = s.smog_score(&grade);
    std::cout << "It's SMOG Readability formula indicator is " << _smog_score_indicator <<
      ", indicating it is at " << smog_grades[grade - 1] << " level.\n";

    s.write_output_file("output.txt", file_name);
  }
}
