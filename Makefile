CC=g++
CFLAGS=-std=c++11 -lboost_system -lboost_filesystem
all:
	$(CC) scorer.cc -o scorer $(CFLAGS)
